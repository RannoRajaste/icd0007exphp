<?php

$numbers = [1, 2, '3', 6, 2, 3, 2, 3];

function countInstances($numbers){

    $nrOfInstances = 0;

    foreach ($numbers as $number) {
        if ($number === 3) {
            $nrOfInstances++;
        }
    }
    return "found it $nrOfInstances times";
}
