<?php

$file = 'data/grades.txt';

$additional_data = ['history' => 5, 'chemistry' => 2];

function addDataToFile($file, $additional_data) {
    $dataString = "";
    foreach ($additional_data as $key => $value){
        $dataString .= "$key;$value\n";
    }
    file_put_contents($file, $dataString, FILE_APPEND);
}

addDataToFile($file, $additional_data);