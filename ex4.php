<?php

$numbers = range(1, 15);
foreach ($numbers as $number){
    if ($number % 3 == 0 && $number % 5 === 0){
        print "FizzBuzz\n";
    } else if ($number % 3 === 0){
        print "Fizz\n";
    } else if ($number % 5 === 0) {
        print "Buzz\n";
    } else {
        print $number;
    }
}
