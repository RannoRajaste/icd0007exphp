<?php

$file = 'data/grades.txt';
function getAverageGrade($fileName){
    $data = file($fileName);
    $gradeSum = 0;
    foreach ($data as $line){
        $gradeSum += explode(";", trim($line))[1];
    }
    return $gradeSum / count($data);
}

